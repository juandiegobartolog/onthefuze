<!--------
Juan Diego Bartolo Gonzalez.
System and Computer Engineer
Mail: juandiegobartolog@gmail.com
Phone: 3206361705
---------->
<!doctype html>
<html lang=es>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width,initial-scale=1" />
<meta name="description" content="DISBRAN">
<meta name="keywords" content=" colombia, pereira, software, onthefueze">
<meta name="author" content="Juan Diego Bartolo Gonzalez - juandiegobartolog@gmail.com" />
<title>ON THE FUZE</title>
<!---------------Jquery------------------->
<script src="lib/jquery-3.5.1.min.js"></script>

<!------------Bootstrap--------------------->
<link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<script src="lib/bootstrap/js/bootstrap.min.js"></script>
<!------------Estilos---------------------->
<link href="css/estilos.css" rel="stylesheet">
<!------------------------------------------>
<link rel="stylesheet" href="lib/wow/css/libs/animate.css">
<!------------------------------------------>
</head>
<body>


    <!-- Modal Javascript-->
    <div id="openModal" class="modalDialog">
      <div>
        <a href="#close" title="Close" class="close" onclick="javascript:CloseModal();">X</a>
        <h1 class="sh2">Example Modal</h1>
            <p class='sp2'>
             The many gentlemen who had been
             boys when she was a girl discovered simultaneously that they loved her,
             and they all ran to her house to propose to her except Mr. Darling, who
             took a cab and nipped in first, and so he got her.
            </p>                
        <button type="button" class="btn btn-secondary" id='btn_close'>Close</button>   
      </div>
    </div>
    <!-- End Modal -->   


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Example Modal 2</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class='row'>
            <div class='col'>
                <h1 class="sh2">Form</h1>
                <!--------------------Ini Form-------------------------->
                    <form role="form" id="form_test">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input name="form_name" id="form_name" type="text" class="form-control" placeholder="First Name*" required data-error="Required">                                    
                                </div>
                             </div>
                             <div class="col-md-6">
                                <div class="form-group">
                                    <input name="form_lastname" id="form_lastname" type="text" class="form-control" placeholder="Last Name*" required data-error="Required">                                    
                                </div>
                             </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input name="form_email" id="form_email" type="email" class="form-control" placeholder="Email*" required data-error="Required" >                                   
                                    </div>
                                 </div>   
                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <input name="form_job" id="form_job" type="text" class="form-control" placeholder="Job Tittle*" required data-error="Required">                                   
                                    </div>
                                </div>
                            </div>
                             <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input name="form_company" id="form_company" type="text" class="form-control" placeholder="Company Name">                                   
                                    </div>
                                 </div>   
                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <input name="form_website" id="form_website" type="text" class="form-control" placeholder="Website URL">                                   
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input name="form_revenue" id="form_revenue" type="text" class="form-control" placeholder="Annual Revenue">    
                                    </div>
                                 </div>   
                            </div>
                            <div class="row">
                            <div class="col">
                                 <input type="submit" id="form-submit" class="btn btn-primary" value="SUBMIT">                                 
                            </div>   
                        </div>    
                    </form>
                <!--------------------End Form-------------------------->
           </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>       
      </div>
    </div>
  </div>
</div>
<!-- End Modal -->
	<!-------------Section 1----------------->
     <div id="section1" class='bckg-1 wow slideInRight' data-wow-duration="4s">
        <div class="container bckg-2">
            <h1 class='titulo'>Lorem <br>Ipsum</h1>
             <br>
        <br>
        <br>
            <div class='row'>

                <div class='col-md-6'>
                    <h1 class='sh1'>The way Mr. Darling won her was this</h1>
                    <p class='sp2'>the many gentlemen who had been
                     boys when she was a girl discovered simultaneously that they loved her,
                     and they all ran to her house to propose to her except Mr. Darling, who
                     took a cab and nipped in first, and so he got her.</p>
                </div>
                <div class='col-md-6' >
                    <!--------------------Ini Form-------------------------->
                    <form role="form" id="form_test">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input name="form_name" id="form_name" type="text" class="form-control" placeholder="First Name*" required data-error="Required">                                    
                                </div>
                             </div>
                             <div class="col-md-6">
                                <div class="form-group">
                                    <input name="form_lastname" id="form_lastname" type="text" class="form-control" placeholder="Last Name*" required data-error="Required">                                    
                                </div>
                             </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input name="form_email" id="form_email" type="email" class="form-control" placeholder="Email*" required data-error="Required" >                                   
                                    </div>
                                 </div>   
                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <input name="form_job" id="form_job" type="text" class="form-control" placeholder="Job Tittle*" required data-error="Required">                                   
                                    </div>
                                </div>
                            </div>
                             <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input name="form_company" id="form_company" type="text" class="form-control" placeholder="Company Name">                                   
                                    </div>
                                 </div>   
                                 <div class="col-md-6">
                                    <div class="form-group">
                                        <input name="form_website" id="form_website" type="text" class="form-control" placeholder="Website URL">                                   
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input name="form_revenue" id="form_revenue" type="text" class="form-control" placeholder="Annual Revenue">    
                                    </div>
                                 </div>   
                            </div>
                            <div class="row">
                            <div class="col">
                                 <input type="submit" id="form-submit" class="btn btn-primary" value="SUBMIT">                                 
                            </div>   
                        </div>    
                    </form>
                <!--------------------End Form-------------------------->
                </div>
            </div>
        </div>
    </div>
     <!-----------End Section1-------------->
     
     <!-----------Section 2-------------->
     <div id="section2" class='bckg-3 wow slideInLeft' data-wow-duration="4s">
        <div class="bckg-4">
           
             <div class="container" id="section2-2">
                <div class='row'>
                    <div Class='col-md-6'>
                        <h2 class="sh2">PETER BREAKS THROUGH</h2>
                        <p class="sp2">All children, except one, grow up. They soon know that they will grow
                        up, and the way Wendy knew was this. One day when she was two years old
                         she was playing in a garden, and she plucked another flower and ran
                        with it to her mother. I suppose she must have looked rather
                         delightful, for Mrs. Darling put her hand to her heart and cried, “Oh,
                         why can’t you remain like this for ever!” This was all that passed
                         between them on the subject, but henceforth Wendy knew that she must
                         grow up. 
                        </p>
                        <div class="row separate-down1">
                            <div class="col">
                                <a href="#" class="btn btn-primary"  id='btn_1'>LOREM IMPSUM</a>
                            </div>
                            <div class="col">
                                <a href="#" class="btn btn-secondary" id='btn_2'>LOREM IMPSUM</a>
                            </div>
                        </div>
                    </div>    
                    <div class='col-md-6 wow bounceInDown' data-wow-duration="4s">
                         <img src='cont/img1.png' class='img-fluid' alt='Team'/>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-----------End Section 2-------------->
     

    <!-----------Section 3-------------->
    <div id="section3" class='bckg-6' >
        <div class='container'>
            <h2 class="sh2 text-center">Of course we can </h2>
            <div id="section3-1" class='container'>
                <p class='sp2'>
                    For a week or two after Wendy came it was doubtful whether they would
                    be able to keep her, as she was another mouth to feed. Mr. Darling was
                    frightfully proud of her, but he was very honourable.
                </p>
            </div>
        </div>
        <!----------- Ini Cards----------->
         <div class='container'>            
            <div class='row'>
                <div class='col-md-4'>
                    <!------------CARD------------->
                    <div class="card text-center wow bounceInLeft" data-wow-duration="2s">
                      <div class='card-obj'></div>
                      <div class="card-body">
                        <h5 class="card-title">Lorem Ipsum</h5>
                        <h6 class="card-subtitle mb-2 text-muted">Lorem Ipsum</h6>
                        <p class="card-text">
                        Mrs. Darling consulted Mr. Darling, but he smiled pooh-pooh. “Mark my
                        words,” he said, “it is some nonsense Nana has been putting into their
                         heads.
                        </p>
                        
                      </div>
                    </div>
                    <!------------CARD------------->
                </div>
                 <div class='col-md-4'>
                    <!------------CARD------------->
                    <div class="card text-center wow bounceInLeft" data-wow-duration="4s">
                        <div class='card-obj'></div>
                      <div class="card-body">
                        <h5 class="card-title">Lorem Ipsum</h5>
                        <h6 class="card-subtitle mb-2 text-muted">Lorem Ipsum</h6>
                        <p class="card-text">
                        I have one pound seventeen here, and two and six at the office; I can
                         cut off my coffee at the office, say ten shillings, making two nine and
                         six, with your eighteen and three makes three nine seven, with five
                         naught naught in my 
                        </p>
                        
                      </div>
                    </div>
                    <!------------CARD------------->
                </div>
                 <div class='col-md-4'>
                    <!------------CARD------------->
                    <div class="card text-center wow bounceInLeft" data-wow-duration="6s">
                        <div class='card-obj'></div>
                      <div class="card-body">
                        <h5 class="card-title">Lorem Ipsum</h5>
                        <h6 class="card-subtitle mb-2 text-muted">Lorem Ipsum</h6>
                        <p class="card-text">
                        “It is so naughty of him not to wipe his feet,” Wendy said, sighing.
                         She was a tidy child.
                        She explained in quite a matter-of-fact way that she thought Peter sometimes.
                        </p>
                        
                      </div>
                    </div>
                    <!------------CARD------------->
                </div>
            </div>
        </div>
        <!----------- End Cards----------->

         

    </div>    
 <!-----------End Section 3-------------->

    
     
    
    
    
    <!-------------Footer--------------------->
    <div id="footer"  class='text-center wow slideInDown' data-wow-duration="2s">
        <div class="container">
            <div class="row">            
                <div class="col">
                    <div class="fnav">
                        <ul class="footer-social">
                            <li><a href="http://www.facebook.com" target="_blank">
                            <i class="fab fa-facebook-square" target="_blank"></i></a></li>
                            <li><a href="http://www.youtube.com" target="_blank">
                            <i class="fab fa-youtube"  target="_blank"></i></a></li>
                            <li><a href="http://www.instagram.com" target="_blank">
                            <i class="fab fa-linkedin-in" target="_blank"></i></a></li>
                         </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
     <!-----------------Footer--------------------->
     
<!---------------Scripts------------------->
<script src="js/pagina.js"></script>
<!--------------WOW------------------------>
<script src="lib/wow/dist/wow.js"></script>
<script src="js/wow_pagina.js"></script>
<!--------------Iconos---------------------->
<script src="https://kit.fontawesome.com/cc73723a89.js" crossorigin="anonymous"></script>
<!----------------------------------------->     
</body>
</html>